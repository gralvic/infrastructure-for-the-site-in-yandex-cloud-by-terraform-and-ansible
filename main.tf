terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.85"
}

provider "yandex" {
  token     = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
  cloud_id  = "xxxxxxxxxxxxxxxxxxxx"
  folder_id = "xxxxxxxxxxxxxxxxxxxx"
}

########## Бастион ##########

resource "yandex_compute_instance" "vm-0" {
  name                      = "bastion"
  zone                      = "ru-central1-c"
  hostname                  = "bastion"
  allow_stopping_for_update = true

  resources {
    cores         = 2
    memory        = 1
    core_fraction = 5
  }

  boot_disk {
    initialize_params {
      image_id = "fd8a67rb91j689dqp60h" #debian11
      size     = 3
      type     = "network-hdd"
    }
  }

  network_interface {
    subnet_id          = yandex_vpc_subnet.subnet-5.id
    nat                = true
    ip_address         = "192.168.50.30"
    security_group_ids = [yandex_vpc_security_group.bastion.id]
  }

  metadata = {
    user-data = "${file("./meta.txt")}"
  }
}

########## ВМ1_Веб-сервер_01 ##########

resource "yandex_compute_instance" "vm-1" {
  name                      = "web01"
  zone                      = "ru-central1-a"
  hostname                  = "web01"
  allow_stopping_for_update = true

  resources {
    cores         = 2
    memory        = 2
    core_fraction = 20
  }

  boot_disk {
    initialize_params {
      image_id = "fd8a67rb91j689dqp60h" #debian11
      size     = 3
      type     = "network-hdd"
    }
  }

  network_interface {
    subnet_id          = yandex_vpc_subnet.subnet-1.id
    nat                = false
    ip_address         = "192.168.10.10"
    security_group_ids = [yandex_vpc_security_group.webservers.id]
  }

  metadata = {
    user-data = "${file("./meta.txt")}"
  }
}

######### ВМ2_Веб-сервер_02 ##########

resource "yandex_compute_instance" "vm-2" {
  name                      = "web02"
  zone                      = "ru-central1-b"
  hostname                  = "web02"
  allow_stopping_for_update = true

  resources {
    cores         = 2
    memory        = 2
    core_fraction = 20
  }

  boot_disk {
    initialize_params {
      image_id = "fd8a67rb91j689dqp60h" #debian11
      size     = 3
      type     = "network-hdd"
    }
  }

  network_interface {
    subnet_id          = yandex_vpc_subnet.subnet-2.id
    nat                = false
    ip_address         = "192.168.20.10"
    security_group_ids = [yandex_vpc_security_group.webservers.id]
  }

  metadata = {
    user-data = "${file("./meta.txt")}"
  }
}

########## ВМ3_Prometheus ##########

resource "yandex_compute_instance" "vm-3" {
  name                      = "prometheus-server"
  zone                      = "ru-central1-c"
  hostname                  = "prometheus-server"
  allow_stopping_for_update = true

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd8a67rb91j689dqp60h" #debian11
      size     = 3
      type     = "network-hdd"
    }
  }

  network_interface {
    subnet_id          = yandex_vpc_subnet.subnet-3.id
    nat                = false
    ip_address         = "192.168.30.10"
    security_group_ids = [yandex_vpc_security_group.prometheus.id]
  }

  metadata = {
    user-data = "${file("./meta.txt")}"
  }
}

########## ВМ4_Elasticsearch ##########

resource "yandex_compute_instance" "vm-4" {
  name                      = "elasticsearch-server"
  zone                      = "ru-central1-c"
  hostname                  = "elasticsearch-server"
  allow_stopping_for_update = true

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd8a67rb91j689dqp60h" #debian11
      size     = 6
      type     = "network-hdd"
    }
  }

  network_interface {
    subnet_id          = yandex_vpc_subnet.subnet-4.id
    nat                = false
    ip_address         = "192.168.40.10"
    security_group_ids = [yandex_vpc_security_group.elasticsearch.id]
  }

  metadata = {
    user-data = "${file("./meta.txt")}"
  }
}

########## ВМ5_Grafana ##########

resource "yandex_compute_instance" "vm-5" {
  name                      = "grafana-server"
  zone                      = "ru-central1-c"
  hostname                  = "grafana-server"
  allow_stopping_for_update = true

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd8a67rb91j689dqp60h" #debian11
      size     = 3
      type     = "network-hdd"
    }
  }

  network_interface {
    subnet_id          = yandex_vpc_subnet.subnet-5.id
    nat                = true
    ip_address         = "192.168.50.10"
    security_group_ids = [yandex_vpc_security_group.grafana.id]
  }

  metadata = {
    user-data = "${file("./meta.txt")}"
  }
}

########## ВМ6_Kibana ##########

resource "yandex_compute_instance" "vm-6" {
  name                      = "kibana-server"
  zone                      = "ru-central1-c"
  hostname                  = "kibana-server"
  allow_stopping_for_update = true

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd8a67rb91j689dqp60h" #debian11
      size     = 6
      type     = "network-hdd"
    }
  }

  network_interface {
    subnet_id          = yandex_vpc_subnet.subnet-5.id
    nat                = true
    ip_address         = "192.168.50.20"
    security_group_ids = [yandex_vpc_security_group.kibana.id]
  }

  metadata = {
    user-data = "${file("./meta.txt")}"
  }
}

########## Сеть и подсети ##########

resource "yandex_vpc_network" "network-1" {
  name = "network1"
}

resource "yandex_vpc_subnet" "subnet-1" {
  name           = "subnet1"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.network-1.id
  v4_cidr_blocks = ["192.168.10.0/24"]
  route_table_id = yandex_vpc_route_table.lab-rt-a.id
}

resource "yandex_vpc_subnet" "subnet-2" {
  name           = "subnet2"
  zone           = "ru-central1-b"
  network_id     = yandex_vpc_network.network-1.id
  v4_cidr_blocks = ["192.168.20.0/24"]
  route_table_id = yandex_vpc_route_table.lab-rt-a.id
}

resource "yandex_vpc_subnet" "subnet-3" {
  name           = "prometheus"
  zone           = "ru-central1-c"
  network_id     = yandex_vpc_network.network-1.id
  v4_cidr_blocks = ["192.168.30.0/24"]
  route_table_id = yandex_vpc_route_table.lab-rt-a.id
}

resource "yandex_vpc_subnet" "subnet-4" {
  name           = "elasticsearch"
  zone           = "ru-central1-c"
  network_id     = yandex_vpc_network.network-1.id
  v4_cidr_blocks = ["192.168.40.0/24"]
  route_table_id = yandex_vpc_route_table.lab-rt-a.id
}

resource "yandex_vpc_subnet" "subnet-5" {
  name           = "public_grafana_kibana"
  zone           = "ru-central1-c"
  network_id     = yandex_vpc_network.network-1.id
  v4_cidr_blocks = ["192.168.50.0/24"]
  route_table_id = yandex_vpc_route_table.lab-rt-a.id
}

########## NAT шлюз ##########

resource "yandex_vpc_gateway" "egress-gateway" {
  name = "egress-gateway"
  shared_egress_gateway {}
}

resource "yandex_vpc_route_table" "lab-rt-a" {
  name       = "potato"
  network_id = yandex_vpc_network.network-1.id
  static_route {
    destination_prefix = "0.0.0.0/0"
    gateway_id         = yandex_vpc_gateway.egress-gateway.id
  }
}

########## Целевая группа ##########

resource "yandex_alb_target_group" "netology-target-group" {
  name = "target-web-servers"

  target {
    subnet_id  = yandex_vpc_subnet.subnet-1.id
    ip_address = yandex_compute_instance.vm-1.network_interface.0.ip_address
  }

  target {
    subnet_id  = yandex_vpc_subnet.subnet-2.id
    ip_address = yandex_compute_instance.vm-2.network_interface.0.ip_address
  }
}

########## Бэкенд группа ##########

resource "yandex_alb_backend_group" "netology-backend-group" {
  name = "backend-web-servers"

  http_backend {
    name             = "netology-http-backend"
    weight           = 1
    port             = 80
    target_group_ids = ["${yandex_alb_target_group.netology-target-group.id}"]
    load_balancing_config {
      panic_threshold = 100
    }
    healthcheck {
      timeout  = "10s"
      interval = "5s"
      http_healthcheck {
        path = "/"
      }
    }
  }
}

########## HTTP-роутер ##########

resource "yandex_alb_http_router" "netology-router" {
  name = "netology-http-router"
  labels = {
    tf-label    = "tf-label-value"
    empty-label = ""
  }
}

resource "yandex_alb_virtual_host" "netology-virtual-host" {
  name           = "netology-vh"
  http_router_id = yandex_alb_http_router.netology-router.id
  route {
    name = "netology-way"
    http_route {
      http_route_action {
        backend_group_id = yandex_alb_backend_group.netology-backend-group.id
        timeout          = "3s"
      }
    }
  }
}

########## Yandex Application Load Balancer ##########

resource "yandex_alb_load_balancer" "netology-balancer" {
  name       = "potato-balancer"
  network_id = yandex_vpc_network.network-1.id

  allocation_policy {
    location {
      zone_id   = "ru-central1-a"
      subnet_id = yandex_vpc_subnet.subnet-1.id
    }

    location {
      zone_id   = "ru-central1-b"
      subnet_id = yandex_vpc_subnet.subnet-2.id
    }
  }

  listener {
    name = "netology-listener"
    endpoint {
      address {
        external_ipv4_address {
        }
      }
      ports = [80]
    }
    http {
      handler {
        http_router_id = yandex_alb_http_router.netology-router.id
      }
    }
  }
}

########## Группа безопасности для бастиона ##########

resource "yandex_vpc_security_group" "bastion" {
  name       = "Bastion security group"
  network_id = yandex_vpc_network.network-1.id
  ingress {
    protocol       = "TCP"
    port           = 22
    v4_cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol       = "ANY"
    v4_cidr_blocks = ["0.0.0.0/0"]
  }
}

########## Группа безопасности для веб-серверов ##########

resource "yandex_vpc_security_group" "webservers" {
  name       = "Webservers security group"
  network_id = yandex_vpc_network.network-1.id

  ingress {
    protocol       = "TCP"
    port           = 22
    v4_cidr_blocks = ["192.168.50.30/32"]
  }

  ingress {
    protocol       = "TCP"
    port           = 80
    v4_cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol       = "TCP"
    port           = 443
    v4_cidr_blocks = ["192.168.50.30/32"]
  }

  ingress {
    protocol       = "TCP"
    port           = 4040
    v4_cidr_blocks = ["192.168.30.10/32"]
  }

  ingress {
    protocol       = "TCP"
    port           = 9100
    v4_cidr_blocks = ["192.168.30.10/32"]
  }

  egress {
    protocol       = "ANY"
    v4_cidr_blocks = ["0.0.0.0/0"]
  }
}

########## Группа безопасности для Prometheus ##########

resource "yandex_vpc_security_group" "prometheus" {
  name       = "Prometheus security group"
  network_id = yandex_vpc_network.network-1.id

  ingress {
    protocol       = "TCP"
    port           = 22
    v4_cidr_blocks = ["192.168.50.30/32"]
  }

  ingress {
    protocol       = "TCP"
    port           = 9090
    v4_cidr_blocks = ["192.168.50.10/32"]
  }

  egress {
    protocol       = "ANY"
    v4_cidr_blocks = ["0.0.0.0/0"]
  }
}

########## Группа безопасности для Grafana ##########

resource "yandex_vpc_security_group" "grafana" {
  name       = "Grafana security group"
  network_id = yandex_vpc_network.network-1.id

  ingress {
    protocol       = "TCP"
    port           = 22
    v4_cidr_blocks = ["192.168.50.30/32"]
  }

  ingress {
    protocol       = "TCP"
    port           = 3000
    v4_cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol       = "ANY"
    v4_cidr_blocks = ["0.0.0.0/0"]
  }
}

########## Группа безопасности для Elasticsearch ##########

resource "yandex_vpc_security_group" "elasticsearch" {
  name       = "Elasticsearch security group"
  network_id = yandex_vpc_network.network-1.id

  ingress {
    protocol       = "TCP"
    port           = 22
    v4_cidr_blocks = ["192.168.50.30/32"]
  }

  ingress {
    protocol       = "TCP"
    port           = 9200
    v4_cidr_blocks = ["192.168.50.20/32", "192.168.10.10/32", "192.168.20.10/32"]
  }

  ingress {
    protocol       = "TCP"
    port           = 9300
    v4_cidr_blocks = ["192.168.50.20/32", "192.168.10.10/32", "192.168.20.10/32"]
  }

  egress {
    protocol       = "ANY"
    v4_cidr_blocks = ["0.0.0.0/0"]
  }
}

########## Группа безопасности для Kibana ##########

resource "yandex_vpc_security_group" "kibana" {
  name       = "Kibana security group"
  network_id = yandex_vpc_network.network-1.id

  ingress {
    protocol       = "TCP"
    port           = 22
    v4_cidr_blocks = ["192.168.50.30/32"]
  }

  ingress {
    protocol       = "TCP"
    port           = 5601
    v4_cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol       = "ANY"
    v4_cidr_blocks = ["0.0.0.0/0"]
  }
}

########## Резервное копирование ##########

resource "yandex_compute_snapshot_schedule" "snapshot" {
  name = "snapshot"

  schedule_policy {
    expression = "00 00 ? * *"
  }

  snapshot_count = 7

  snapshot_spec {
    description = "snapshot-everyday"
  }

  disk_ids = ["${yandex_compute_instance.vm-0.boot_disk.0.disk_id}", "${yandex_compute_instance.vm-1.boot_disk.0.disk_id}", "${yandex_compute_instance.vm-2.boot_disk.0.disk_id}", "${yandex_compute_instance.vm-3.boot_disk.0.disk_id}", "${yandex_compute_instance.vm-4.boot_disk.0.disk_id}", "${yandex_compute_instance.vm-5.boot_disk.0.disk_id}", "${yandex_compute_instance.vm-6.boot_disk.0.disk_id}"]
}

resource "local_file" "ssh" {
  content  = <<-EOT
Host bastion
Hostname ${yandex_compute_instance.vm-0.network_interface.0.nat_ip_address}
IdentityFile ~/.ssh/id_ed25519
StrictHostKeyChecking no

Host web01
Hostname 192.168.10.10
IdentityFile ~/.ssh/id_ed25519
StrictHostKeyChecking no
ProxyCommand ssh ${yandex_compute_instance.vm-0.network_interface.0.nat_ip_address} -W %h:%p

Host web02
Hostname 192.168.20.10
IdentityFile ~/.ssh/id_ed25519
StrictHostKeyChecking no
ProxyCommand ssh ${yandex_compute_instance.vm-0.network_interface.0.nat_ip_address} -W %h:%p

Host prometheus
Hostname 192.168.30.10
IdentityFile ~/.ssh/id_ed25519
StrictHostKeyChecking no
ProxyCommand ssh ${yandex_compute_instance.vm-0.network_interface.0.nat_ip_address} -W %h:%p

Host elasticsearch
Hostname 192.168.40.10
IdentityFile ~/.ssh/id_ed25519
StrictHostKeyChecking no
ProxyCommand ssh ${yandex_compute_instance.vm-0.network_interface.0.nat_ip_address} -W %h:%p

Host grafana
Hostname 192.168.50.10
IdentityFile ~/.ssh/id_ed25519
StrictHostKeyChecking no
ProxyCommand ssh ${yandex_compute_instance.vm-0.network_interface.0.nat_ip_address} -W %h:%p

Host kibana
Hostname 192.168.50.20
IdentityFile ~/.ssh/id_ed25519
StrictHostKeyChecking no
ProxyCommand ssh ${yandex_compute_instance.vm-0.network_interface.0.nat_ip_address} -W %h:%p
   EOT
  filename = "/home/gralvic/.ssh/config"
}
